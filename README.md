# Pets REST API

[SpringBoot](./logo.svg)



```
GET /pets           - Get All the Pets
GET /pets/{id}      - Get a Single Pet
POST /pets          - Create a Pet
PATCH /pets/{id}    - Update a Pet
DELETE /pets/{id}   - Delete a Pet
```
